package jobBoardProject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class activity7_createNewJob {
	WebDriver driver;
	WebDriverWait wait;
	  
  @Test
  public void createJobTest() {
	  //Navigate to post a job
	  Reporter.log("Navigating to Post a Job");
	  driver.findElement(By.linkText("Post a Job")).click();
	  
	  //sign in
	  driver.findElement(By.cssSelector("a.button")).click();
	  driver.findElement(By.id("user_login")).sendKeys("root");
	  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
	  WebElement loginSubmit = driver.findElement(By.id("wp-submit"));
	  wait.until(ExpectedConditions.visibilityOf(loginSubmit));
	  loginSubmit.click();
	  
	//Input form details
	  driver.findElement(By.id("job_title")).sendKeys("Data Analyst");
	  driver.findElement(By.id("job_location")).sendKeys("Banglore");
	  //Job Type dropdown 
	  Select jobTypeSelect = new Select(driver.findElement(By.id("job_type")));
	  jobTypeSelect.selectByIndex(1);
	  
	  //Switching to iframe
	  Reporter.log("Switching to Iframe");
	  driver.switchTo().frame("job_description_ifr");
	  driver.findElement(By.id("tinymce")).sendKeys("Looking for 5 years of professional experience");
	  driver.switchTo().defaultContent();
	  
	  //Form input
	  WebElement application = driver.findElement(By.id("application"));
	  application.clear();
	  application.sendKeys("jobs@techgroup.com");
	  WebElement companyName = driver.findElement(By.id("company_name"));
	  companyName.clear();
	  companyName.sendKeys("Tech Group");
	  WebElement website = driver.findElement(By.id("company_website"));
	  website.clear();
	  website.sendKeys("www.techgroup.com");
	  WebElement tagLine = driver.findElement(By.id("company_tagline"));
	  tagLine.clear();
	  tagLine.sendKeys("Cognitive Company");
	  WebElement twitter = driver.findElement(By.id("company_twitter"));
	  twitter.clear();
	  twitter.sendKeys("@techgrp");
	  
	  //Finding preview button and click
	  Reporter.log("Click on preview button");
	  driver.findElement(By.cssSelector("input.button")).click();
	  
	  //Opening Preview page
	  wait.until(ExpectedConditions.elementToBeClickable(By.id("job_preview_submit_button")));
	  Reporter.log("Click on Submit Listing");
	  driver.findElement(By.id("job_preview_submit_button")).click();  
	  Reporter.log("Job posted successfully");
	  WebElement postSuccess = driver.findElement(By.xpath("//div[@class='entry-content clear']"));
	  System.out.println(postSuccess.getText());
	  
	  // Verification of job listing posted by visiting the jobs page
	  // is not done in this test, as it needs to access the backend website and approve.
	  
  }
  @BeforeClass
  public void browserOpen() {
	  driver = new FirefoxDriver();
	  wait = new WebDriverWait(driver,30);
	  driver.get("https://alchemy.hguy.co/jobs/");
  }

  @AfterClass
  public void browserClose() {
	  driver.close();
  }

}
