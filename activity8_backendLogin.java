package jobBoardProject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;

public class activity8_backendLogin {
	WebDriver driver;
	WebDriverWait wait;
  @Test
  public void websiteBackendLogin() {
	  //Login details
	  Reporter.log("Logging in to backend website");
	  driver.findElement(By.id("user_login")).sendKeys("root");
	  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
	  wait.until(ExpectedConditions.elementToBeClickable(By.id("wp-submit")));
	  driver.findElement(By.id("wp-submit")).click();
	  Reporter.log("Login success");
	  Reporter.log("Fetching new page title");
	  System.out.println(driver.getTitle());
	  Assert.assertEquals("Dashboard � Alchemy Jobs � WordPress", driver.getTitle());
	  Reporter.log("Testcase is passed");
  }
  
  @BeforeTest
  public void beforeTest() {
	  driver = new FirefoxDriver();
	  wait = new WebDriverWait(driver,20);
	  driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	   }

  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
