package jobBoardProject;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class pageTitleVerify1 {
	WebDriver driver;
	
  @Test
  public void pageTitle() {
	  Reporter.log("Verifying Title of the website");
	 System.out.println("Title is: " +driver.getTitle());
	 Assert.assertEquals("Alchemy Jobs � Job Board Application", driver.getTitle());
	 Reporter.log("Testcase is passed");
  }
  
  @BeforeClass (alwaysRun=true)
  public void browserOpen() {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/jobs/");
	 
  }

  @AfterClass(alwaysRun=true)
  public void browserClose() {
	  driver.close();
  }

}
