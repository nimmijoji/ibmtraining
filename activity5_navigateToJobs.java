package jobBoardProject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;

public class activity5_navigateToJobs {
	WebDriver driver;
	WebDriverWait wait;
	
  @Test
  public void navigateToJobsTest() {
	  Reporter.log("Navigate to Jobs link");
	  driver.findElement(By.linkText("Jobs")).click();
	  Reporter.log("Verifying page title in new page");
	  System.out.println("The new page title is:" +driver.getTitle());
	  Assert.assertEquals("Jobs � Alchemy Jobs", driver.getTitle());
	  Reporter.log("Testcase is passed");
  }
  @BeforeTest
  public void browserOpen() {
	  driver = new FirefoxDriver();
	  wait = new WebDriverWait(driver,10);
	  driver.get("https://alchemy.hguy.co/jobs/");
  }

  @AfterTest
  public void browserClose() {
	  driver.close();
  }

}
