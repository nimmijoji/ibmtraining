package jobBoardProject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;

public class imageURLtest3 {
	WebDriver driver;
	
  @Test
  public void imageURLverification() {
	  WebElement url = driver.findElement(By.xpath("//img[contains(@class,'attachment-large')]"));
	  Reporter.log("Printing header image URL of the website to console");
	  System.out.println("The header image URL is " +url.getAttribute("src"));
	  Reporter.log("Testcase is passed");
  }
  @BeforeTest
  public void browserOpen() {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/jobs/");
  }

  @AfterTest
  public void browserClose() {
	  driver.close();
  }

}
