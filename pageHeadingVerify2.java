package jobBoardProject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;

public class pageHeadingVerify2 {
	WebDriver driver;
  @Test
  public void pageHeading() {
	  Reporter.log("Verifying Heading of the website");
	  WebElement heading = driver.findElement(By.xpath("//h1[@class='entry-title']"));
	  System.out.println("Website heading is: " +heading.getText());
	  Assert.assertEquals("Welcome to Alchemy Jobs", heading.getText());
	  Reporter.log("Testcase is passed");
  }
  @BeforeTest
  public void browserOpen() {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/jobs/");
  }

  @AfterTest
  public void browserClose() {
	  driver.close();
  }

}
