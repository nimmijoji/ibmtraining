package jobBoardProject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class activity4_secondHeadingTest {
	WebDriver driver;
	
  @Test
  public void secondHeadingVerify() {
	  WebElement secondHead = driver.findElement(By.tagName("h2"));
	  System.out.println("Second page heading is "+secondHead.getText());
	  Reporter.log("Verifying second heading of the website");
	  Assert.assertEquals("Quia quis non", secondHead.getText());
	  Reporter.log("Testcase is passed");
  }
  @BeforeClass
  public void browserOpen() {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/jobs/");
  }

  @AfterClass
  public void browserClose() {
	  driver.close();
  }

}
