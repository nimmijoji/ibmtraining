package jobBoardProject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class activity9_createJobBackend {
	WebDriver driver;
	WebDriverWait wait;
	Actions action;
	
	 @Test(priority=0)
	  public void websiteBackendLogin() {
		  //Login details
		  Reporter.log("Logging in to backend website");
		  driver.findElement(By.id("user_login")).sendKeys("root");
		  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		  wait.until(ExpectedConditions.elementToBeClickable(By.id("wp-submit")));
		  driver.findElement(By.id("wp-submit")).click();
		  Reporter.log("Login success");
		  driver.manage().window().maximize();
		  Reporter.log("Fetching new page title");
		  System.out.println(driver.getTitle());
		  Assert.assertEquals("Dashboard � Alchemy Jobs � WordPress", driver.getTitle());
		  Reporter.log("Testcase is passed");
	  }
	 
  @Test(priority=1)
  public void createNewJob() {
	  //Navigate to Job LIstings
	  WebElement jobList = driver.findElement(By.cssSelector("a.menu-icon-job_listing"));
	  action.moveToElement(jobList).perform();
	 
	  //Click on Add New
	  Reporter.log("Clicking on Add New");
	  wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Add New")));
	  driver.findElement(By.linkText("Add New")).click();
	  
	  //Posting job
	  Reporter.log("Form input");
	  WebElement position = driver.findElement(By.id("post-title-0"));
	  position.sendKeys("Oracle DBA Admin");
	  WebElement textBox = driver.findElement(By.xpath("//div[starts-with(@id,'editor-')]"));
	  wait.until(ExpectedConditions.elementToBeClickable(textBox));
	  textBox.click();
	  textBox.sendKeys("Looking for software professional with 7 years of experience in Oracle");
	  driver.findElement(By.id("_company_website")).sendKeys("www.techgroup.com");
	  driver.findElement(By.id("_company_twitter")).sendKeys("@techgrp");
	  driver.findElement(By.id("_job_location")).sendKeys("Banglore");
	  driver.findElement(By.id("_company_name")).sendKeys("TechGroup");
	  
	  //Clicking on Publish button
	  Reporter.log("Clicking on Publish button");
	  driver.findElement(By.xpath("//button[contains(@class,'-panel__toggle editor-post-publish-button')]")).click();
	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class,'-button editor-post-publish-button')]")));
	  driver.findElement(By.xpath("//button[contains(@class,'-button editor-post-publish-button')]")).click();
	  
	  Reporter.log("Publish is success");
	  wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Oracle DBA Admin")));
	  WebElement published =  driver.findElement(By.xpath("//div[@class='editor-post-publish-panel__header-published']"));
	  System.out.println(published.getText());	
	  Assert.assertEquals("Published", published.getText());
	  
	  //Verifying job listed
	  driver.findElement(By.linkText("Oracle DBA Admin")).click();
	  WebElement title = driver.findElement(By.className("entry-title"));
	  Assert.assertEquals("Oracle DBA Admin", title.getText());
	  Reporter.log("Job Listed successfully");
  }
  @BeforeClass
  public void browserOpen() {
	  driver = new FirefoxDriver();
	  wait = new WebDriverWait(driver,40);
	  action = new Actions(driver);
	  driver.get("https://alchemy.hguy.co/jobs/wp-admin");
  }

  @AfterClass
  public void browserClose() {
	 driver.close();
	  
  }

}
