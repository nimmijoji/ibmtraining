package jobBoardProject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class activity10_createNewUser {
	WebDriver driver;
	WebDriverWait wait;
	Actions actions;
	
	 @Test(priority=0)
	  public void websiteBackendLogin() {
		  //Login details
		  Reporter.log("Logging in to backend website");
		  driver.findElement(By.id("user_login")).sendKeys("root");
		  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		  wait.until(ExpectedConditions.elementToBeClickable(By.id("wp-submit")));
		  driver.findElement(By.id("wp-submit")).click();
		  driver.manage().window().maximize();
		  Reporter.log("Fetching new page title");
		  System.out.println(driver.getTitle());
		  Assert.assertEquals("Dashboard  Alchemy Jobs  WordPress", driver.getTitle());
		  Reporter.log("Testcase is passed");
	  }
  @Test(priority=1)
  public void createNewUser() {
	  //Navigate to Users
	  WebElement user = driver.findElement(By.xpath("//div[@class='wp-menu-image dashicons-before dashicons-admin-users']"));
	  actions.moveToElement(user).perform();
	  
	  Reporter.log("Clicking on Add New User");
	  wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Add New")));
	  driver.findElement(By.linkText("Add New")).click();
	  
	  //Input Add new user form
	  Reporter.log("Input Add New user form");
	  driver.findElement(By.id("user_login")).sendKeys("njoj123");
	  driver.findElement(By.name("email")).sendKeys("njoj1950@gmail.com");
	  driver.findElement(By.id("first_name")).sendKeys("Nim");
	  driver.findElement(By.name("last_name")).sendKeys("Joji");
	  driver.findElement(By.id("url")).sendKeys("www.techgrp.com");
	  
	  //password button
	  driver.findElement(By.xpath("//button[contains(@class,'wp-generate-pw')]")).click();
	  WebElement pwdButton = driver.findElement(By.id("pass1"));
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pass1")));
	  pwdButton.clear();
	  String pwd = "@Test234hjQ";
	  ((JavascriptExecutor) driver).executeScript("arguments[0].value='" +pwd + "';", pwdButton);
	  
	  driver.findElement(By.id("send_user_notification")).isSelected();
	  Assert.assertEquals(true,true);
	  
	  //Select from Role drop down
	  Reporter.log("Changing role from dropdown");
	  WebElement role = driver.findElement(By.id("role"));
	  role.click();
	  Select selectedRole = new Select(role);
	  selectedRole.selectByIndex(0);
	  
	  Reporter.log("Clicking Add User button");
	  driver.findElement(By.cssSelector("input#createusersub")).click();
	  
	  //Verify user is created
	  Reporter.log("Verify user added successfully");
	  WebElement addSuccess = driver.findElement(By.cssSelector("div#message"));
	  System.out.println(addSuccess.getText());
	  String message = "New user created. Edit user\nDismiss this notice.";
	  Assert.assertEquals(message, addSuccess.getText());
  }
  
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  wait = new WebDriverWait(driver,40);
	  actions = new Actions(driver);
	  driver.get("https://alchemy.hguy.co/jobs/wp-admin");
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
