package jobBoardProject;

import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;

import org.testng.annotations.BeforeClass;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class activity12_createUser {
	WebDriver driver;
	WebDriverWait wait;
	Actions action;
	
	@Test(priority=0)
	  public void websiteBackendLogin() {
		  //Login details
		  Reporter.log("Logging in to backend website");
		  driver.findElement(By.id("user_login")).sendKeys("root");
		  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		  wait.until(ExpectedConditions.elementToBeClickable(By.id("wp-submit")));
		  driver.findElement(By.id("wp-submit")).click();
		  driver.manage().window().maximize();
	  }
	
	@Test(priority=1)
	public void userCreate()  {
	//Navigate to Users
	  WebElement user = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[11]/a/div[2]"));
	  action.moveToElement(user).perform();
	  
	  Reporter.log("Clicking on Add New User");
	  wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Add New")));
	  driver.findElement(By.linkText("Add New")).click();
	}	
  @Test(priority=2)
  public void createUserCSV() throws CsvException, IOException {
	// Read CSV file
      
         CSVReader reader = new CSVReader(new FileReader("C:\\Users\\NimmiJoji\\eclipse-workspace\\Selenium Proj\\resources\\Activity12_testdata.csv.txt"));
          String[] cell;
     
          while ((cell = reader.readNext()) != null) {
        	  
        	  for(int i=0;i<1;i++) {
              String Username = cell[i];
              String email = cell[i + 1];
              String firstName = cell[i + 2];
              String lastName = cell[i + 3];
              String website = cell[i + 4];
              String pwd = cell[i + 5];
              String role = cell[i + 6];
          
              driver.findElement(By.id("user_login")).sendKeys(Username);
              driver.findElement(By.id("email")).sendKeys(email);
              driver.findElement(By.id("first_name")).sendKeys(firstName);
              driver.findElement(By.id("last_name")).sendKeys(lastName);
              driver.findElement(By.id("url")).sendKeys(website);
              
              //password button click
              driver.findElement(By.xpath("//button[contains(@class,'wp-generate-pw')]")).click();
        	  WebElement pwdButton = driver.findElement(By.id("pass1"));
        	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pass1")));
        	  pwdButton.clear();
        	  
        	  ((JavascriptExecutor) driver).executeScript("arguments[0].value='" +pwd + "';", pwdButton);
        	  driver.findElement(By.id("role")).sendKeys(role);
        	  
        	  //Add user click
              Reporter.log("Clicking Add User button");
        	  driver.findElement(By.cssSelector("input#createusersub")).click();
        	  
        	  //Verify user is created
        	  Reporter.log("Verify user added successfully");
        	  WebElement addSuccess = driver.findElement(By.cssSelector("div#message"));
        	  System.out.println(addSuccess.getText());
        	  String message = "New user created. Edit user\nDismiss this notice.";
        	  Assert.assertEquals(message, addSuccess.getText());
        	  }
          }
  }
  
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  wait = new WebDriverWait(driver,40);
	  action = new Actions(driver);
	  driver.get("https://alchemy.hguy.co/jobs/wp-admin"); 
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
