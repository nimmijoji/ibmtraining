package jobBoardProject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class activity11_jobSearch {
	WebDriver driver;
	WebDriverWait wait;
	
  @Test
  public void jobSearch() {
	  //Navigate to Jobs
	  Reporter.log("Navigate to Jobs link");
	  driver.findElement(By.linkText("Jobs")).click();
	  
	  //keyword search input
	  driver.findElement(By.id("search_keywords")).sendKeys("Oracle");
	  driver.findElement(By.xpath("//input[@type='submit']")).click();
	  
	  //check box selections
	  WebElement freelanceCheck = driver.findElement(By.xpath("//input[@id='job_type_freelance']"));
	  freelanceCheck.click();
	  System.out.println("Is Freelance checkbox selected: " +freelanceCheck.isSelected());
	  
	  Reporter.log("Job Type Full time check box is selected");
	  WebElement jobTypeCheck = driver.findElement(By.xpath("//input[@id='job_type_full-time']"));
	  System.out.println("Is Job type checkbox selected: " +jobTypeCheck.isSelected());
	  
	  WebElement internshipCheck = driver.findElement(By.xpath("//input[@id='job_type_internship']"));
	  internshipCheck.click();
	  System.out.println("Is Internship checkbox selected: " +internshipCheck.isSelected());
	  
	  WebElement partTimeCheck = driver.findElement(By.xpath("//input[@id='job_type_part-time']"));
	  partTimeCheck.click();
	  System.out.println("Is Part time checkbox selected: " +partTimeCheck.isSelected());
	  
	  WebElement tempCheck = driver.findElement(By.xpath("//input[@id='job_type_temporary']"));
	  tempCheck.click();
	  System.out.println("Is temporary checkbox selected: " +tempCheck.isSelected());
	  
	  //Click on Search Jobs button
	  WebElement searchJobs = driver.findElement(By.xpath("//input[@type='submit']"));
	  wait.until(ExpectedConditions.visibilityOf(searchJobs));
	  Reporter.log("Clicking Search jobs");
	  searchJobs.click();
	  
	  //click and open job listed
	  Reporter.log("Click and open job listed");
	  WebElement jobList = driver.findElement(By.xpath("//ul[@class='job_listings']")); 
	  wait.until(ExpectedConditions.visibilityOfAllElements(jobList));
	  driver.findElement(By.xpath("//ul/li[1][contains(@class,'type-job_listing')]")).click();
	  
	  //Finding job page title
	  WebElement title = driver.findElement(By.className("entry-title"));
	  System.out.println("Page title is: " +title.getText());
	  Reporter.log("Search Job Listed successfully");
	  
	  
	  //Clicking on Apply for Job
	  Reporter.log("Clicking on Apply for Job");
	  WebElement applyJob = driver.findElement(By.xpath("//input[@type='button']"));
	  wait.until(ExpectedConditions.visibilityOf(applyJob));
	  applyJob.click();	  
	  System.out.println("Job applied successfuly");
	  Reporter.log("Apply job is succes");
  }
  
  @BeforeClass
  public void browserOpen() {
	  driver = new FirefoxDriver();
	  wait = new WebDriverWait(driver,30);
	  driver.get("https://alchemy.hguy.co/jobs/");
  }

  @AfterClass
  public void browserClose() {
	  driver.close();
  }

}
