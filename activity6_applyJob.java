package jobBoardProject;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class activity6_applyJob {
	WebDriver driver;
	WebDriverWait wait;
  @Test
  public void applyJobTest() {
	  //Navigate to Jobs and enter and search
	  Reporter.log("Navigate to Jobs link");
	  driver.findElement(By.linkText("Jobs")).click();
	  Reporter.log("Enter the job");
	  driver.findElement(By.id("search_keywords")).sendKeys("Java");
	  Reporter.log("Locating Search jobs");
	  WebElement searchJobs = driver.findElement(By.xpath("//input[@type='submit']"));
	  wait.until(ExpectedConditions.visibilityOf(searchJobs));
	  Reporter.log("Clicking Search jobs");
	  searchJobs.click();
	  
	  //click and open job listed
	   Reporter.log("Click and open job listed");
	  WebElement jobList = driver.findElement(By.cssSelector("ul.job_listings")); 
	  wait.until(ExpectedConditions.visibilityOfAllElements(jobList));
	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul/li[2][contains(@class,'type-job_listing')]")));
	  driver.findElement(By.xpath("//ul/li[2][contains(@class,'type-job_listing')]")).click();
	  
	  //Clicking on Apply for Job
	  Reporter.log("Clicking on Apply for Job");
	  WebElement applyJob = driver.findElement(By.xpath("//input[@type='button']"));
	  wait.until(ExpectedConditions.visibilityOf(applyJob));
	  applyJob.click();
	  
	  //Email details
	  WebElement mailDetails = driver.findElement(By.cssSelector("div.application_details"));
	  wait.until(ExpectedConditions.visibilityOf(mailDetails));
	  
	  Reporter.log("Printing the email to console");
	  WebElement mailTo = driver.findElement(By.cssSelector("a.job_application_email"));
	  System.out.println("The mail id is " +mailTo.getText());  
  }
  
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  wait = new WebDriverWait(driver,30);
	  driver.get("https://alchemy.hguy.co/jobs/");
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
